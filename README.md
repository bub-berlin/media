Medienvorlagen für Ortsgruppen
=============================

Dieses Repo umfasst Medienvorlagen für lokale Ortsgruppen, auf Basis des offiziellen Corporate Designs von https://bits-und-baeume.org/material/de

**-> Download aller Medien-Dateien [hier](https://gitlab.com/bub-berlin/media/-/archive/master/media-master.zip
) [zip].**

Vorschau der Dateien
----------------

### Logo ohne Text

<img src="B&B_Logo_RGB-leaf-only-no-text.svg.png" width="200">

### Profilbild mit Ort, z. B. für Mastodon

![](B&B_Logo_RGB-Berlin-300px-white-bg.png)

### Banner, z. B. für Header im Mastodon Profil

![](B&B_Header-Mastodon.png)

### Praesentation Vorlage

*Tipp*: Die Präsentations-Vorlage wurde mit LibreOffice erstellt und ist am besten auch mit dieser Software kompatibel. Individuelle Inhalte im Hintergrund, wie z. B. "Berlin" können im Folienmaster auf z. B. Dresden angepasst werden.

<img style="border: 2px solid black" src="ressources/screenshot-presentation-first-slide.png" width="600">

<img style="border: 2px solid black" src="ressources/screenshot-presentation-content.png" width="600">


Tipps:
---------
- Für das Editieren der Vektorgraphiken bietet sich die Software Inkscape an.

Schriftarten (fonts)
---------
Die hier z. T. verwendete Schriftart weicht von der im Original ab, weil unbekannt ist, welche Schriftart die Designer von [Schauschau](https://schauschau.cc/) ursprünglich verwendet haben. Die Schriftart ist in keiner der Originaldateien eingebettet.
Für vom Original abgeleiteten Werke/Designs wurde die Schriftart "Arimo Regular" verwendet ([download hier](https://www.1001fonts.com/arimo-font.html#styles)). Diese Schriftart wurde unter der Apache 2.0 Lizenz veröffentlicht.


Lizenz
------------
CC BY Schauschau.cc
